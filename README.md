# Задание ITMO2023 Drone Line Follower
<p><a href="mailto:roboforces@yandex.ru"><img src="https://custom-icon-badges.demolab.com/badge/-roboforces@yandex.ru-2a2b2e?style=for-the-badge&amp;logo=mention&amp;logoColor=white" style='width:19.2%;' alt="Mail"/></a><a href="https://t.me/competition_roboforce2023"><img src="https://img.shields.io/badge/Telegram-2CA5E0?style=for-the-badge&logo=telegram&logoColor=white" style='width:10%;' alt="Mail"/></a><a href="https://abit.itmo.ru/file_storage/file/pages/26/roboforce.pdf"><img src="https://img.shields.io/badge/%D0%9F%D1%80%D0%B0%D0%B2%D0%B8%D0%BB%D0%B0%20%D0%BF%D1%80%D0%BE%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D0%B8%D1%8F-2B579A.svg?style=for-the-badge&logo=Microsoft-Word&logoColor=white" style='width:17.8%;' alt="Mail"/></a>
</p>

### Конкурс Roboforce 2023. Победа в конкурсе дает право на поступления без вступительных экзаменов на программу магистратуры: "Робототехника и искусственный интеллект"
---
![control pic](https://i.imgur.com/MD1NiNe.png)
---

## Сроки
Сроки проведения конкура: **с 12.05.2023 18.00**(МСК) по **22.05.2023 23.59**(МСК)  
Регистрация доступна: **с 6.05.2023 18.00**(МСК) по **21.05.2023 23.59**(МСК)

<iframe src="https://free.timeanddate.com/countdown/i8ttarvw/n1927/cf100/cm0/cu4/ct0/cs0/ca0/co1/cr0/ss0/cace05e45/cpc000/pcfff/tcfff/fn3/fs150/szw728/szh108/tat%D0%97%D0%B0%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5%20%D1%81%D1%82%D0%B0%D0%BD%D0%B5%D1%82%20%D0%B4%D0%BE%D1%81%D1%82%D1%83%D0%BF%D0%BD%D0%BE%20%D1%87%D0%B5%D1%80%D0%B5%D0%B7%3A/tac111/tpc090/iso2023-05-12T18:00:00" allowtransparency="true" frameborder="0" width="728" height="108"></iframe>


## Правила отбора победителей
Итоговые очки конкурса для поступления рассчитываются относительно лучшего результата (лучший результат приравнивается к 100 баллам). Процент или абсолютное число победителей будут определены после завершения процесса регистрации.

## Задача
Репозиторий содержит ROS-пакет с примером *управления* квадрокоптером и получением *данных с камер*. Участнику следует, модифицируя этот пакет, решить задачу.

Дана гоночная трасса с рельефной поверхностью. По центру трассы нанесена замкнутая направляющая Линия. Над трассой могут встречаться кольца, при пролёте сквозь которые можно заработать бонусные баллы, если кольца синего цвета, или штраф, если кольца красного цвета.

![drone pic](https://i.imgur.com/gFHLvut.png)

Участникам предлагается, с использованием доступного робота и камер на нём, реализовать алгоритм управления, который позволит следовать вдоль Линии трассы, избегая красных (штрафных) колец и залетая в синие (бонусные) кольца.

Квадрокоптер оснащен датчиками: RGB-камера вертикальная (изображение под дроном), RGB-камера горизонтальная (изображение перед дроном), сонар, альтиметр, барометр, компас, инерциальный измерительный модуль.

Требуется разработать техническое решение, включая алгоритмическое и программное обеспечения системы управления и обработки сенсорной информации, в форме программного пакета для ROS на языках программирования С++ и/или Python.

## Тесты

В закрытых тестовых сценариях могут быть изменены конфигурация рельефа поверхности, его фон, форма и параметры трассы, высота и расположение колец.

![scene pic](https://i.imgur.com/8OSPOxv.png)

* квадрокоптер работает в условиях постоянного нормального освещения, при этом не исключаются ветровые нагрузки ограниченной амплитуды, действие которых может быть учтено и скомпенсировано за счет управляющих воздействий;
* форма и параметры Линии являются неизвестными и не могут быть извлечены из моделей симулятора и/или заданы из файлов конфигурации или непосредственно в исходном коде;
* параметры и расположение Колец являются неизвестными и не могут быть извлечены из моделей симулятора и/или заданы из файлов конфигурации или непосредственно в исходном коде;
* боковые границы Линии всегда могут быть определены бортовой камерой с учетом ее ограниченного поля зрения
* после прохождения квадрокоптером Кольца, оно замораживается и повторное его прохождение возможно только на следующем круге;

## Важные замечания

* При реализации алгоритмов управления рекомендуется использоваться время симуляции `rostime`;

* Движение квадрокоптера по трассе должно осуществляться *по часовой стрелке*. Модель квадрокоптера создается в сцене так, что "перед" квадрокоптера направлен в сторону, куда должно быть начато движение (т.е. при создании модели квадрокоптера в сцене ось X системы координат `base_link` квадрокоптера направлена в сторону движения вдоль трассы по часовой стрелке).


## Как все работает

Для решения задачи доступны четыре read-only docker-образа:

- [base] `registry.gitlab.com/roboforces-itmo/open-tasks/drone-line-follower/dlf-solution/base:ubuntu-latest` -- включает все зависимости.
- [base-gpu] `registry.gitlab.com/roboforces-itmo/open-tasks/drone-line-follower/dlf-solution/base:nvidia-latest ` -- также включает все зависимости, с использованием GPU.

- [scene] `registry.gitlab.com/roboforces-itmo/open-tasks/drone-line-follower/dlf-solution/scene:ubuntu-latest` -- собран на основе 'base' предыдущего и дополнительно включает файлы сцены в gazebo.
- [scene-gpu] `registry.gitlab.com/roboforces-itmo/open-tasks/drone-line-follower/dlf-solution/scene:ubuntu-latest` -- собран на основе 'base-gpu' и дополнительно включает файлы сцены в gazebo.

Запуск включает два шага:
- В контейнере сервиса `scene` на основе образа `[scene]`/`[scene-gpu]` запускается сцена в симуляторе gazebo [dlf_scene](https://gitlab.com/roboforces-itmo/open-tasks/drone-line-follower/dlf-scene).
- В контейнере сервиса `solution` на основе образа `[base]`/`[base-gpu]` запускается решение [dlf_solution](https://gitlab.com/roboforces-itmo/open-tasks/drone-line-follower/dlf-solution).

Для автоматизации запуска запуска docker-контейнеров используется инструмент docker compose. Описание параметров запуска доступно в: `docker-compose.yml ` или `docker-compose.nvidia.yml`

<!-- *Note! Если вы используется систему с GPU от Nvidia, доступны версии образов с тегом `nvidia-latest` и `docker-compose.nvidia.yml`x* -->


## Установка и настройка окружения

Для настройки окружения необходимо иметь одну из перечисленных операционных систем:
1. Ubuntu 18.04 и старше
2. Windows 10 и старше, с установленным WSL (Не рекомендуется).

Для подготовки окружения необходимо сделать следующее:
1. Установить docker-engine: [Docker Engine](https://docs.docker.com/engine/install/ubuntu/).  
2. Также необходимо установить docker-compose-plugin: [Docker Compose](https://docs.docker.com/compose/install/linux/).  
3. Если вы планируете использовать видеокарту, установите также nviidia-container-toolkit: [Nvidia Container Toolkit](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html)
4. Добавить в группу docker пользователя

    ```bash
    sudo groupadd docker
    sudo usermod -aG docker $USER
    newgrp docker
    ```


## Как запустить начальное(базовое решение)
**Сделать форк репозитория** в корень gitlab своего юзера, **не изменяя имени репозитория**.  
Склонировать репозиторий: [БАЗОВОЕ РЕШЕНИЕ](https://gitlab.com/roboforces-itmo/open-tasks/drone-line-follower/dlf-solution) 

```bash
git clone https://gitlab.com/<YOUR_NAME>/dlf-solution.git
cd dlf-solution
```

Дать права для подключения группе docker к дисплею хоста:

```
xhost +local:docker
```

Запустить сцену и ros-пакет из этого репозитория:

```bash
docker compose -f docker-compose.yml up --build --pull always
```
Или если у вас видеокарта Nvidia:
```bash
docker compose -f docker-compose.nvidia.yml up --build --pull always
```

*Note!* В файле `docker-compose.yml` хранится описание параметров запуска сцены и решения. По умолчанию запускается `simple_move`

```bash
rosrun dlf-soluton simple_move
```

### Редактирование базового решения
Для редактирования доступны все файлы в репозтории, за исключение файлов `docker-compose*.yml`.  
Чтобы начать решать задание вы можете отредактировать файл `solution.launch` выбрав запуск python или C++ версии программы решения.

Если вы пишете на python, нужно, чтобы в `solution.launch` была раскомментирована строка:

    <node name="simple_move" pkg="dlf-solution" type="simple_move.py" output="screen" />

Если вы пишете на C++, нужно, чтобы в `solution.launch` была раскомментирована строка:

    <node name="simple_move" pkg="dlf-solution" type="simple_move" output="screen" />

## Выгрузка решения на сервер
Для отправки на проверку вашего решения необходимо на сайте заполнить следующие поля:
- __Href to repository__: ссылка на ваш репозиторий(форк), скопированная из браузера,
- __Repo token__: в первой строке имя токена, во второй сам токен. [Как получить токен?](https://docs.gitlab.com/ee/user/project/deploy_tokens/)

## Особенности запуска решений на сервере
На данный момент сервере отсутствует GUI, поэтому:
**[ВАЖНО]** При выгрузке решения для проверки убедитесь, что при установке __GUI=false__ в `docker-compose*.yml` ваше решение не создает никаких окон, для удобства разработки, в примерах решения есть переменная, получаемая из ros parameters, передающая значение переменной __GUI__ в программу.

## Дополнительные полезные команды
В случае необходимости пересборки используйте флаг `--build`:

    docker compose -f docker-compose.yml up --build

Для получения последней версии сцены (обновления) используейте флаг `--pull always`:

    docker compose -f docker-compose.yml up --build --pull always

### Подключение в контейнер

Для открытия новой bash-сессии в сервисе решения: `solution` используйте команду:

    docker compose exec solution bash

Для открытия новой bash-сессии в сервисе сцены: `scene` используйте команду:

    docker compose exec scene bash

### Рестарт сцены или решения по отдельности
Для перезапуска **решения** используйте:

    docker compose restart solution

Для перезапуска **сцены** используйте:

    docker compose restart scene


## Оценивание
Время, которое дается роботу на выполнение задания **10 минут**.
- За каждое **синее кольцо** начисляется от **0 до 10 баллов** в зависимости от качества следования по линии;
- За каждое **красное кольцо** забирается **5 баллов**;
- При вылете за пределы трассы, баллы ануллируются, а попытка считается завершенной.
